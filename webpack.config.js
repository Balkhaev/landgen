const fs = require('fs');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const sites = fs.readdirSync(__dirname + '/sites').reduce((acc, cur) => {
  const sitePath = `${__dirname}/sites/${cur}`;

  if (cur.startsWith('.') || !fs.lstatSync(sitePath).isDirectory()) {
    return acc;
  }


  acc.entries[cur + '/script'] = sitePath + '/script.js';
  acc.html.push(
    new HtmlWebpackPlugin({
      chunks: [cur + '/script'],
      filename: cur + '/page.html',
      template: sitePath + '/page.html'
    })
  )

  return acc;
}, { entries: {}, html: [] });

module.exports = {
  entry: sites.entries,
  output: {
    path: __dirname + '/dist',
    filename: '[name].[hash].js'
  },
  plugins: [
    new CleanWebpackPlugin(__dirname + '/dist'),
    ...sites.html,
  ]
}
